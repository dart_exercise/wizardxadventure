import 'Object.dart';

class Monster extends Object {
  late int x, y;
  late int hp;
  late int atk = 10;
  Monster(int x, int y, int hp) : super("X", x, y) {
    this.x = x;
    this.y = y;
    this.hp = hp;
  }

  int getHP() {
    return this.hp;
  }

  int setHP(int hp) {
    this.hp = hp;
    return hp;
  }

  int getAtk() {
    return this.atk;
  }
}
